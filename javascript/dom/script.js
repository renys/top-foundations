const container = document.querySelector('#container');
const content = document.createElement('div');

content.classList.add('content');
content.textContent = 'This is the glorious text-content!';

container.appendChild(content);

// Red p
const para = document.createElement('p');
para.setAttribute('style', 'color: red;');
para.textContent = "Hey I'm red!";

container.appendChild(para);

// Blue h3
const header = document.createElement('h3');
header.setAttribute('style', 'color: blue');
header.textContent = "I'm a blue h3!";

container.appendChild(header);

// Black/pink div
const blackpink = document.createElement('div');
blackpink.setAttribute('style', 'background-color: pink; border: solid black;');

const headerB = document.createElement('h1');
headerB.textContent = "I'm in a div";
const pB = document.createElement('p');
pB.textContent = "ME TOO!";

blackpink.appendChild(headerB);
blackpink.appendChild(pB);

container.appendChild(blackpink);
