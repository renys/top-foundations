const choices = ["rock", "paper", "scissors"];
let playerScore = 0, computerScore = 0;

function computerPlay() {
    return choices[Math.floor(Math.random() * 3)];
}

function isDraw(playerSelection, computerSelection) {
    return playerSelection === computerSelection;
}

function isWinner(playerSelection, computerSelection) {
    switch(playerSelection) {
        case "rock":
            return computerSelection === "scissors";
        case "paper":
            return computerSelection === "rock";
        case "scissors":
            return computerSelection === "paper";
    }
}

function playRound(playerSelection, computerSelection) {
    playerSelection = playerSelection.toLowerCase();
    let draw = isDraw(playerSelection, computerSelection);
    let winner = isWinner(playerSelection, computerSelection);

    if (draw) {
        return "You've drawn! You both chose " + playerSelection; 
    } else if (winner) {
        playerScore++;
        return "You won! " + playerSelection + " beats " + computerSelection; 
    } else {
        computerScore++;
        return "You lost! " + playerSelection + " is beaten by " + computerSelection;
    }
}

function game() {
    let playerSelection = "what", computerSelection = "";

    for (let round = 1; round <= 5; round++) {
        console.log("Round " + round);
        do {
            playerSelection = prompt("Choose rock, paper, or scissors!");
            console.log(playerSelection);
        } while (!choices.includes(playerSelection.toLowerCase()));
        computerSelection = computerPlay();
        console.log(playRound(playerSelection, computerSelection));

        console.log("Player: " + playerScore + " || Computer: " + computerScore);
    }
}

game();