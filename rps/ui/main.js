const choices = ["rock", "paper", "scissors"];
let playerScore = 0, computerScore = 0;

function computerPlay() {
    return choices[Math.floor(Math.random() * 3)];
}

function isDraw(playerSelection, computerSelection) {
    return playerSelection === computerSelection;
}

function isWinner(playerSelection, computerSelection) {
    switch(playerSelection) {
        case "rock":
            return computerSelection === "scissors";
        case "paper":
            return computerSelection === "rock";
        case "scissors":
            return computerSelection === "paper";
    }
}

function isGameOver(pScore, comScore) {
    return (pScore === 5 || comScore === 5);
}

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function playRound(playerSelection, computerSelection) {
    playerSelection = playerSelection.toLowerCase();
    let draw = isDraw(playerSelection, computerSelection);
    let winner = isWinner(playerSelection, computerSelection);

    if (draw) {
        return "You've drawn! You both chose " + playerSelection; 
    } else if (winner) {
        playerScore++;
        return "You won the round! " + capitalizeFirstLetter(playerSelection) + " beats " + computerSelection; 
    } else {
        computerScore++;
        return "You lost the round! " + capitalizeFirstLetter(playerSelection) + " is beaten by " + computerSelection;
    }
}

function refreshScore() {
    playerScoreDiv.textContent = playerScore;
    computerScoreDiv.textContent = computerScore;
}

const buttons = document.querySelectorAll('button');
const playerScoreDiv = document.querySelector('.player-score');
const computerScoreDiv = document.querySelector('.computer-score');
const infoDiv = document.querySelector('.result');

buttons.forEach((button) => {
    button.addEventListener('click', () => {
        if (!isGameOver(playerScore, computerScore)) {
        // console.log(button.id);
        computerSelection = computerPlay();
        let winnerString = playRound(button.id, computerSelection);

        if (!isGameOver(playerScore, computerScore)) {
            infoDiv.textContent = winnerString;
        } else {
            infoDiv.textContent = (playerScore === 5) ? 'Player wins! Congratulations!' : 'Computer wins! Better luck next time.';
        }
        // console.log("Player: " + playerScore + " || Computer: " + computerScore);

        refreshScore();
        
        }
    })
});

// first time seeing the site? here ya go
window.onload = () => {
    refreshScore(); 
    infoDiv.textContent = 'Choose rock, paper, or scissors!';
}
