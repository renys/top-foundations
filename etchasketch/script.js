// const/default vars
const grid = document.querySelector('#grid');
const site = document.querySelector('#main');
const slider = document.querySelector('#widthRange');
const colourButton = document.querySelector('#mode-toggle');
const visibleWidth = document.querySelector('.visible-width');
const visibleMode = document.querySelector('#current-mode');
console.log(visibleWidth);
const DEFAULT_WIDTH = 48;
const DEFAULT_MODE = 'bw';

// changable vars
let width = DEFAULT_WIDTH;
let mode = DEFAULT_MODE;
let gridCells = document.querySelector('.cell');

colourButton.onclick = (e) => changeMode();
slider.onmousemove = (e) => changeWidthText(e.target.value);
slider.onchange = (e) => changeWidth(e.target.value);

function changeWidthText(newText) {
    visibleWidth.textContent = `${newText}x${newText}`;
}

function changeWidth(newWidth) {
    width = newWidth;
    redrawGrid();
}

function redrawGrid() {
    clearGrid();
    drawBoard(width);
}

function changeMode() {
    mode = (mode === "bw" ? "colour" : "bw");
    visibleMode.textContent = `${mode === "bw" ? "Black and White" : "Colour"}`;
}

function clearGrid() {
    grid.innerHTML = '';
}

function generateRandomColour() {
    let color = '#';
    for (let i = 0; i < 6; i++){
       const random = Math.random();
       const bit = (random * 16) | 0;
       color += (bit).toString(16);
    };
    return color;
}

function changeColour(cell) {
    let colour = "";
    switch(mode) {
        case "bw":
            colour = "black";
            break;
        case "colour":
            colour = generateRandomColour();
            break;
        default:
            colour = "#dddddd";
            break;
    }
    cell.target.setAttribute('style', `background-color: ${colour};`);
}

function drawBoard(w) {
    grid.setAttribute('style', `grid-template-columns: repeat(${w}, 1fr); grid-template-rows: repeat(${w}, 1fr);`);
    
    changeWidthText(w);
    
    for (let i = 0; i < w**2; i++) {
        const cell = document.createElement('div');
        cell.classList.add('cell');
        cell.addEventListener("mouseover", changeColour);
        grid.appendChild(cell);
    }
}

window.onload = () => {
    drawBoard(width);
    visibleMode.textContent = `${mode === "bw" ? "Black and White" : "Colour"}`;
} 
